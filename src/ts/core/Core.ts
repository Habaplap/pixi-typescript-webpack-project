import * as PIXI from 'pixi.js';
import Stage from './../ui/Stage';

export default class Core{

    private _renderer:any;
    private _stage:Stage;
    private _ticker:PIXI.ticker.Ticker;
    private _cube:PIXI.Graphics;

    constructor() {
        this.initializeRenderer();
        this.initializeStage();
        this.initializeTicker();
        this.addEventListener();

        this.start();

        this.resize();
    }

    private initializeRenderer():void{
        this._renderer = PIXI.autoDetectRenderer(window.innerWidth, window.innerHeight, {antialias: true, transparent: false, resolution: window.devicePixelRatio});
        document.getElementById('mainApp').appendChild(this._renderer.view);
        this._renderer.autoResize = true;
    }

    private initializeStage():void{
        this._stage = new Stage();
    }

    private initializeTicker():void{
        this._ticker = PIXI.ticker.shared;
        this._ticker.minFPS = 30;
        this._ticker.autoStart = true;
    }

    private addEventListener():void{
        window.onresize = (event:Event) => {this.resize(event);};
        this._ticker.add(this.update, this);
    }

    private resize(event:Event = null):void{
        this._renderer.resize( window.innerWidth, window.innerHeight );

        /**
         * example handle resize event on cube
         */

        this._cube.x = window.innerWidth * 0.1;
        this._cube.y = window.innerHeight * 0.1
    }

    private update():void{
        this._renderer.render(this._stage);
    }


    /**
     *
     * Start with your game here
     *
     */
    private start():void{
        /**
         * create example Cube
         */

        this._cube = new PIXI.Graphics();
        this._cube.beginFill( 0x333333);
        this._cube.drawRect(0, 0, 200, 200);
        this._cube.endFill();

        this._stage.addChild(this._cube);
    }
    
}