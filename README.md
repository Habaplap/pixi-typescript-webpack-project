PIXI Typescript Webpack Blank Project
==============================

This is a blank project with a babel, webpack setup to to start programming a HTML5 game in Typescript and PIXI.

PIXI is a 2d Javascript Renderer which support WEBGL and Canvas. PIXI also provides many Helper Classes 
like Sprite, BitmapText, Loaders etc. (for more information visit https://pixijs.github.io/docs/).
 
Webpack is a Module Bundler which is able to combine your sources you used in your project( for example jade files, 
coffee script files or even images and sounds) and create one Javascript bundle out of it. Webpack comes with many 
features like a dev Server or a Filewatcher (for more information visit https://webpack.github.io/).

GULP is a task runner for Javascript which allows you to create and configure task you need to deploy or prepare your
project by running a console command (for mor information visit http://gulpjs.com/).

Setup
=====

After the successful fork of the project start 'npm install' to get all needed node packages.
    
    npm install

Please check if 'npm install' has generated the 'typing folder in the the root node. If not please run following command:

    npm run install_typings

or

    typings install

After the npm install is finished. Move the files which are in the staticFiles folder to the public folder 
(creates the folder the public folder when needed). Start the webpack filewatcher 'with npm dev'.

    npm run dev
    
    
You should be able to now access the game by typing 'localhost:8200' in your browser.
You should see a greyish example rectangle.

To build your project run:

    npm run build

Start
=====

When you've setup the project and see the test rectangle in your browser you are able to start programming your game.
The PIXI renderer is already setup and the render loop is running. Your entry point is the 'start'-function
in the src/ts/Core.ts class. Remove the testCube and all his references if you don't need the cube anymore.

   