const path = require('path');
const merge = require('webpack-merge');
const webpack = require('webpack');
const TARGET = process.env.npm_lifecycle_event;
const CopyWebpackPlugin = require('copy-webpack-plugin');

const common = {
    cache:false,
    context: __dirname + '/src',
     /*entry: [
        'webpack-dev-server/client?http://0.0.0.0:8200', // WebpackDevServer host and port
        'webpack/hot/only-dev-server',
        './src/index.ts' // Your appʼs entry point
    ],*/
    entry: {
        app: './src/ts/index.ts'
    },
    output: {
        path: './public',
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['', '.ts', '.d.ts', '.webpack.js','.web.js', '.js'],
        root: path.join(__dirname, "src"),
        moduleDirectories: [
            "node_modules"
        ]
    },
    node: {  // this is for pixi.js
        fs: "empty"
    },
    module: {
        loaders: [
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                loader: 'ts-loader'
            },
            {
                test: /\.json$/,
                exclude: /node_modules/,
                loader: 'json-loader'
            }
        ],
        noParse: [/.pixi.js$/]
    }
};

if(TARGET === 'watch' || TARGET === 'dev' || !TARGET) {
    module.exports = merge(common, {
        context: path.join(__dirname, ''),
        devServer: {
            contentBase: './public/',
            historyApiFallback: true,
            hot: true,
            inline: true,
            progress: true,
            stats: 'errors-only',
            host: process.env.HOST,
            port: 8200
        },
        cache: true,
        devtool: 'cheap-module-source-map',
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new webpack.DefinePlugin({
                'process.env': {
                'NODE_ENV': JSON.stringify('production')
                }
            })
            
        ]
    });
}

if(TARGET === 'build') {
    module.exports = merge(common, {
        context: path.join(__dirname, '')
    });
}